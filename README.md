# L1TGCEvaluation


## clone
<pre>
asetup 21.3.5,Athena
lsetup git
git clone ssh://git@gitlab.cern.ch:7999/hhibi/L1tgcevaluation.git
</pre>

## build
<pre>
L1tgcevaluation  
mkdir build  run
cd build

//In build
cmake ../L1tgcevaluation
cd ../run
cp ../build/src/L1TGCEva .
</pre>


## run
<pre>
./L1TGCEva -BWCW pathToCW -Input pathToNtuple

-BWCW : path of CW for BW.
-Input : file path and name.(L1TGCNtuple)
</pre>

example
'./L1TGCEva -BWCW ../../TurnOnCurve/athena/Trigger/TrigT1/TrigT1TGC/share/BW_CW_Run3Format/ -Input group.det-muon/group.det-muon.19069326.L1TGCNtuple._000105.root'





