#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>

#include "TGCNSWCoincidenceMap.h"
#include "TGCNSWOut.h"




  TGCNSWCoincidenceMap::TGCNSWCoincidenceMap(const std::string& NSWCWpath)
    : m_NSWCWpath(NSWCWpath)
  {
    // intialize map
    for (size_t side=0; side< N_Side; side++){
      for (size_t sec=0; sec<N_TGCTRIGGERSECTOR_Endcap; sec++){
        for (size_t roi=0; roi<N_RoI_Endcap ; roi++){
          for(size_t ptlevel=0; ptlevel!=N_PT_THRESH; ptlevel++){
            Endcap_Flag_EtaPhi[side][sec][roi][ptlevel]=false;
            Endcap_Flag_EtaDtheta[side][sec][roi][ptlevel]=false;
            if(sec<N_TGCTRIGGERSECTOR_Forward && roi<N_RoI_Forward){
              Forward_Flag_EtaPhi[side][sec][roi][ptlevel]=false;
              Forward_Flag_EtaDtheta[side][sec][roi][ptlevel]=false;
            }
          }
        }
      }
    }

    std::string kEndModule[6] = {"0a", "1a", "3a", "4a", "6a", "7a"}; 
    std::string kForModule[6] = {"2a", "5b", "8a", "2b", "5a", "8b"}; 

    //---------Read out CW data---------
    this->readMap( kEndModule, true, ReadCW_Type::EtaPhi_CW);  
    this->readMap( kForModule, false, ReadCW_Type::EtaPhi_CW); 
    this->readMap( kEndModule, true, ReadCW_Type::EtaDtheta_CW);  
    this->readMap( kForModule, false, ReadCW_Type::EtaDtheta_CW); 
    //---------Fill Shift data---------
    this->readShift();
  }


  TGCNSWCoincidenceMap::TGCNSWCoincidenceMap()
  {}

  TGCNSWCoincidenceMap::~TGCNSWCoincidenceMap()
  {}


  //TGC-NSW Eta&Phi Coincidence
  int TGCNSWCoincidenceMap::TGCNSW_pTcalcu_EtaPhi(const TGCNSWOut *NSWOut,int sideId,TGCRegionType region, int sectorId,int RoI) const
  {

    std::vector<int> nswR_vec=NSWOut->GetNSWeta();
    std::vector<int> nswPhi_vec=NSWOut->GetNSWphi();
    int highest_pT=0;
    for(unsigned int R_id=0;R_id!=nswR_vec.size();R_id++){
      if(region==ENDCAP){
        int R_decode=Endcap_Shift[sideId][sectorId][RoI][0]-nswR_vec[R_id]+32;
        int Phi_decode=Endcap_Shift[sideId][sectorId][RoI][1]-nswPhi_vec[R_id]+8;

        if(highest_pT<Endcap_CW[sideId][sectorId][RoI][R_decode][Phi_decode]){
          highest_pT=Endcap_CW[sideId][sectorId][RoI][R_decode][Phi_decode];
        }
      }
      if(region==FORWARD){
        int R_decode=-nswR_vec[R_id]+Forward_Shift[sideId][sectorId][RoI][0]+32;
        int Phi_decode=-nswPhi_vec[R_id]+Forward_Shift[sideId][sectorId][RoI][1]+8;
	std::cout<<"DR="<<R_decode<<"  DPhi="<<Phi_decode<<std::endl;
        if(highest_pT<Forward_CW[sideId][sectorId][RoI][R_decode][Phi_decode]){
          highest_pT=Forward_CW[sideId][sectorId][RoI][R_decode][Phi_decode];
        }
      }
    }

    return highest_pT;
  }


  //TGC-NSW Eta-DeltaTheta Coincidence
  int TGCNSWCoincidenceMap::TGCNSW_pTcalcu_EtaDtheta(const TGCNSWOut *NSWOut,int sideId,TGCRegionType region, int sectorId,int RoI) const
  {
    std::vector<int> nswR_vec=NSWOut->GetNSWeta();
    std::vector<int> nswDtheta_vec=NSWOut->GetNSWDtheta();
    int highest_pT=0;
    for(unsigned int R_id=0;R_id!=nswR_vec.size();R_id++){
      if(region==ENDCAP){
        int R_decode=Endcap_Shift[sideId][sectorId][RoI][0]-nswR_vec[R_id]+32;
        int Dtheta_decode=nswDtheta_vec[R_id];
	std::cout<<"DR="<<R_decode<<"  DTheta="<<Dtheta_decode<<std::endl;
        if(highest_pT<Endcap_etaDthetaCW[sideId][sectorId][RoI][R_decode][Dtheta_decode]){
          highest_pT=Endcap_etaDthetaCW[sideId][sectorId][RoI][R_decode][Dtheta_decode];
        }
      }
      if(region==FORWARD){
        int R_decode=Forward_Shift[sideId][sectorId][RoI][0]-nswR_vec[R_id]+32;
        int Dtheta_decode=nswDtheta_vec[R_id];
	std::cout<<"DR="<<R_decode<<"  DTheta="<<Dtheta_decode<<std::endl;
        if(highest_pT<Forward_etaDthetaCW[sideId][sectorId][RoI][R_decode][Dtheta_decode]){
          highest_pT=Forward_etaDthetaCW[sideId][sectorId][RoI][R_decode][Dtheta_decode];
        }
      }
    }

    return highest_pT;
  }




  bool TGCNSWCoincidenceMap::getFlag(int pt,int side,TGCRegionType region,int sector,int roi) const
  {
    if(region==ENDCAP){
      return Endcap_Flag_EtaPhi[side][sector][roi][pt-1]&&Endcap_Flag_EtaDtheta[side][sector][roi][pt-1];
      //if true, require NSW coincidence
    }
    if(region==FORWARD){
      return Forward_Flag_EtaPhi[side][sector][roi][pt-1]&&Forward_Flag_EtaDtheta[side][sector][roi][pt-1];
      //if true, require NSW coincidence
    }

    return true;
  }
  

  bool TGCNSWCoincidenceMap::readMap(std::string kModule[6], bool isEndcap, ReadCW_Type cw_type) 
  {
    std::string kSide[2] = {"a", "c"};
    std::string kCWtype[2] = {"EtaPhi","EtaDtheta"};
    for(int side=0;side!=2;side++){
      for(int module=0;module!=6;module++){
        for(int octant=0;octant!=8;octant++){
          std::string dbname="";
          dbname = "cm_" + kSide[side] + kModule[module]+kCWtype[cw_type]+"_Octant_v0001.db";
	  std::string fullName = m_NSWCWpath+dbname;
          if(octant==0){std::cout<<dbname<<std::endl;}
          //----- 
          std::ifstream data(fullName);
          if(!data.is_open()){return false;}
          char delimiter = '\n';
          std::string field;
          std::string tag;
          while (std::getline(data, field, delimiter)) {
            int RoI=-1;
            unsigned int N_Rbit=0, N_Rbit2=0;
            unsigned int N_HOGEbit=0, N_HOGEbit2=0;
            std::istringstream header(field); 
            header >> tag;
            if(tag=="#"){ // read header part.     
              header >> RoI >> N_Rbit >> N_Rbit2 >>N_HOGEbit >> N_HOGEbit2 ;
            }
            // get trigger word
            std::string word;
            unsigned int pT=0;
            //*********Endcap CW***********
            if(isEndcap){
              int TGC_TriggerSector =module + 6*octant;
              for(size_t posR=0; posR<N_dR ; posR++){
                std::getline(data, field, delimiter);
                std::istringstream cont(field);
                //----Read EtaPhi CW---------
                for(size_t posPHI=0; posPHI<N_dPHI && cw_type==ReadCW_Type::EtaPhi_CW; posPHI++){
                  cont >> word;
                  std::istringstream(word) >> std::hex >> pT;
                  Endcap_CW[side][TGC_TriggerSector][RoI][posR][posPHI]=pT;
                  if(pT!=0){
                    for(size_t pT_thre=N_PT_THRESH;pT_thre!=pT-1&& !Endcap_Flag_EtaPhi[side][TGC_TriggerSector][RoI][pT-1] ;pT_thre--){
                      Endcap_Flag_EtaPhi[side][TGC_TriggerSector][RoI][pT_thre-1]=true;
                    }
                  }
                }

                //-----Read EtaDtheta CW-----
                for(size_t posDTHETA=0; posDTHETA<N_Dtheta && cw_type==ReadCW_Type::EtaDtheta_CW; posDTHETA++){
                  cont >> word;
                  std::istringstream(word) >> std::hex >> pT;
                  Endcap_etaDthetaCW[side][TGC_TriggerSector][RoI][posR][posDTHETA]=pT;
                  if(pT!=0){
                    for(size_t pT_thre=N_PT_THRESH;pT_thre!=pT-1&& !Endcap_Flag_EtaDtheta[side][TGC_TriggerSector][RoI][pT-1] ;pT_thre--){
                      Endcap_Flag_EtaDtheta[side][TGC_TriggerSector][RoI][pT_thre-1]=true;
                    }
                  }
                }

              }
            } 

            //************Forward CW**********
            if(octant%2==0 &&  !isEndcap){
              int TGC_TriggerSector =module + 3*octant;
              //----Read EtaPhi CW---------
              for(size_t posR=0; posR<N_dR; posR++){
                std::getline(data, field, delimiter);
                std::istringstream cont(field);
                for(size_t posPHI=0; posPHI<N_dPHI && cw_type==ReadCW_Type::EtaPhi_CW; posPHI++){
                  cont >> word;
                  std::istringstream(word) >> std::hex >> pT;
                  Forward_CW[side][TGC_TriggerSector][RoI][posR][posPHI]=pT;
                  if(pT!=0){
                    for(size_t pT_thre=N_PT_THRESH;pT_thre!=pT-1&& !Forward_Flag_EtaPhi[side][TGC_TriggerSector][RoI][pT-1] ;pT_thre--){
                      Forward_Flag_EtaPhi[side][TGC_TriggerSector][RoI][pT_thre-1]=true;
                    }
                  }
                }
              
                //-----Read EtaDtheta CW-----
                for(size_t posDTHETA=0; posDTHETA<N_Dtheta && cw_type==ReadCW_Type::EtaDtheta_CW; posDTHETA++){
                  cont >> word;
                  std::istringstream(word) >> std::hex >> pT;
                  Forward_etaDthetaCW[side][TGC_TriggerSector][RoI][posR][posDTHETA]=pT;
                  if(pT!=0){
                    for(size_t pT_thre=N_PT_THRESH;pT_thre!=pT-1&& !Forward_Flag_EtaDtheta[side][TGC_TriggerSector][RoI][pT-1] ;pT_thre--){
                      Forward_Flag_EtaDtheta[side][TGC_TriggerSector][RoI][pT_thre-1]=true;
                    }
                  }
                }
              }
            }
           
          }
          data.close();
        }
      }
    }

    return true;
  } 
  
  
  bool TGCNSWCoincidenceMap::readShift()
  {
    // select right database according to a set of thresholds

    //----- 
    char delimiter = '\n';
    std::string field;
    std::string tag;
    int side;
    int TriggerSector;

    //------- Read Endcap Shift
    std::string dbname="";
    dbname = "../L1tgcevaluation/share/RoIpos_Endcap.db";
    std::cout<<dbname<<std::endl;
    std::ifstream data(dbname);
    if(!data.is_open()){return false;}

    while (std::getline(data, field, delimiter)) {
      std::istringstream header(field); 
      header >> tag;
      if(tag=="#"){ // read header part.
	header >> side >> TriggerSector ;
      }
  
      // get trigger word
      std::getline(data, field, delimiter);
      std::istringstream cont(field);
      std::string word;
      int SHIFT=0;
      int phi_shift[4];
      int eta_shift[37];
  
      for(size_t phiN=0; phiN!=4; phiN++){
	cont >> word;
	std::istringstream(word) >> SHIFT;
	phi_shift[phiN]=SHIFT;
      }
      for(size_t etaN=0; etaN!=37; etaN++){
	cont >> word;
	std::istringstream(word) >> SHIFT;
	eta_shift[etaN]=SHIFT;
      }
      for(size_t roi=0;roi!=N_RoI_Endcap;roi++){
	Endcap_Shift[side][TriggerSector][roi][0]=eta_shift[roi/4];
	Endcap_Shift[side][TriggerSector][roi][1]=phi_shift[roi%4];
      }
    }
    data.close();

    //------- Read Forward Shift
    dbname = "../L1tgcevaluation/share/RoIpos_Forward.db";
    std::cout<<dbname<<std::endl;
    std::ifstream data2(dbname);
    if(!data2.is_open()){return false;}

    while(std::getline(data2, field, delimiter)) {
      std::istringstream header(field); 
      header >> tag;
      if(tag=="#"){ // read header part.
	header >> side >> TriggerSector ;
	std::cout<<"side="<<side<<" "<<"Sector="<<TriggerSector<<std::endl;
      }
      // get trigger word
      std::getline(data2, field, delimiter);
      std::istringstream cont(field);
      std::string word;
      int SHIFT=0;
      int phi_shift[4];
      int eta_shift[16];

      for(size_t phiN=0; phiN!=4; phiN++){
	cont >> word;
	std::istringstream(word) >> SHIFT;
	phi_shift[phiN]=SHIFT;
	std::cout<<SHIFT<<" ";
      }
      for(size_t etaN=0; etaN!=16; etaN++){
	cont >> word;
	std::istringstream(word) >> SHIFT;
	eta_shift[etaN]=SHIFT;
	std::cout<<SHIFT<<" ";
      }
      std::cout<<std::endl;
      for(size_t roi=0;roi!=N_RoI_Forward;roi++){
	Forward_Shift[side][TriggerSector][roi][0]=eta_shift[roi/4];
	Forward_Shift[side][TriggerSector][roi][1]=phi_shift[roi%4];
      }
    }

    return true;
  }


